# APTE: A PineTime Environment

#-----------------------------------------------------------------------
# Variables which are not user-controllable and must not change
#-----------------------------------------------------------------------

# APTE lives here, we'll need it in other Makefiles
export APTE_DIR := $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

#-----------------------------------------------------------------------
# The first target in a Makefile is the default one. Let's make it
# phony and default to ' help'
#-----------------------------------------------------------------------

.PHONY: help

help:
	@printf "\n"
	@printf "Targets:\n"
	@printf "\n"
	@printf "%-17.17s %s\n" "help" "Show this help"
	@printf "%-17.17s %s\n" "keys" "Generate signing keys"
	@printf "%-17.17s %s\n" "micro_ecc" "Build the micro-ECC library"
	@printf "%-17.17s %s\n" "bootloader.hex" "Build the bootloader"
	@printf "%-17.17s %s\n" "flash_erase_all" "Erase the flash"
	@printf "%-17.17s %s\n" "flash_softdevice" "Flash the SoftDevice"
	@printf "%-17.17s %s\n" "flash_bootloader" "Flash the bootloader"
	@printf "%-17.17s %s\n" "flash_erase_and_flash_all" "Erase then flash the SoftDevice bootloader"
	@printf "%-17.17s %s\n" "application.hex" "Build the application"
	@printf "%-17.17s %s\n" "application.zip" "Package the application for DFU"
	@printf "%-17.17s %s\n" "bootloader.zip" "Package the bootloader for DFU"
	@printf "%-17.17s %s\n" "clean_application" "Clean the application build files"
	@printf "%-17.17s %s\n" "clean_bootloader" "Clean the bootloader build files"
	@printf "%-17.17s %s\n" "clean_micro_ecc" "Clean the micro-ECC library build files"
	@printf "%-17.17s %s\n" "clean" "Clean all build files"
	@printf "\n"
	@printf "Variables:\n"
	@printf "\n"
	@printf "%-17.17s %-48.48s %s\n" "GNU_INSTALL_ROOT" "Location of GNU ARM toolchain binaries" "[$(GNU_INSTALL_ROOT)]"
	@printf "%-17.17s %-48.48s %s\n" "SD_NUM" "Number (132, 140, ...) of the SoftDevice to use" "[$(SD_NUM)]"
	@printf "%-17.17s %-48.48s %s\n" "SD_VER" "Version of the SoftDevice to use" "[$(SD_VER)]"
	@printf "\n"

#-----------------------------------------------------------------------
# User-controllable options
#-----------------------------------------------------------------------

# The Ubuntu arm-none-eabi toolchain lives in /usr/bin
export GNU_INSTALL_ROOT ?= /usr/bin/

# By default we build for softdevice S132 version 7.0.1
SD_NUM ?= 132
SD_VER ?= 7.0.1

# Default bootloader version is 1.
# Define it to a greater value in order to package an updated bootloader
BL_VER ?= 1

# The bootloader's default DFU name is "DfuTarg". Change that.
export BL_BLE_NAME := "DfuPineTime"

#-----------------------------------------------------------------------
# These internal variables derive from the user-controllable ones
#-----------------------------------------------------------------------

BL_HEX_FILE_PATH := pinetime/bootloader/_build/nrf52832_xxaa_s$(SD_NUM).hex

SD_HEX_FILE_PATH := \
 nrfsdk/components/softdevice/s$(SD_NUM)/hex/s$(SD_NUM)_nrf52_$(SD_VER)_softdevice.hex

#-----------------------------------------------------------------------
# Signing (secret and public) keys
#-----------------------------------------------------------------------

# The secret key must be generated if it does not exist already
# Make it non-writable to cause a warning in case it is rm'ed
secret_key.pem:
	@nrfutil keys generate $@
	@chmod a-w $@
	@echo "*******************************************************"
	@echo "*                                                     *"
	@echo "*  YOU HAVE JUST CREATED THE SECRET KEY.              *"
	@echo "*                                                     *"
	@echo "*  If you lose this key, and if you have used this    *"
	@echo "*  key to create a bootloader, then any watch which   *"
	@echo "*  has this bootloader will be FOREVER LOCKED:        *"
	@echo "*  - you won't be able to update its application;     *"
	@echo "*  - you won't be able to update its bootloader;      *"
	@echo "*  - you won't be able to update its SoftDevice.      *"
	@echo "*                                                     *"
	@echo "*  SO SAVE A COPY OF THE FILE 'secret_key.pem' NOW !  *"
	@echo "*                                                     *"
	@echo "*******************************************************"
	@echo ""
	@read -p "Press enter to confirm that you have read the above: " DUMMY
	@echo ""

# The public key derives from the secret key
# Make it non-writable to cause a warning in case it is rm'ed
public_key.c: secret_key.pem
	@nrfutil keys display --key pk --format code $^ > $@
	@chmod a-w $@

.PHONY: keys

keys: secret_key.pem public_key.c

#-----------------------------------------------------------------------
# micro_ecc is a crypto library, needed for secure bootloader
#-----------------------------------------------------------------------

nrfsdk/external/micro-ecc/micro-ecc/.git:
	@git submodule update --init nrfsdk/external/micro-ecc/micro-ecc

nrfsdk/external/micro-ecc/nrf52hf_armgcc/armgcc/micro_ecc_lib_nrf52.a: \
 nrfsdk/external/micro-ecc/micro-ecc/.git
	@make -s -C nrfsdk/external/micro-ecc/nrf52hf_armgcc/armgcc

.PHONY: micro_ecc clean_micro_ecc

micro_ecc: \
 nrfsdk/external/micro-ecc/nrf52hf_armgcc/armgcc/micro_ecc_lib_nrf52.a

clean_micro_ecc:
	@make -s -C nrfsdk/external/micro-ecc/nrf52hf_armgcc/armgcc clean
	@rm -f \
 nrfsdk/external/micro-ecc/nrf52hf_armgcc/armgcc/micro_ecc_lib_nrf52.a

#-----------------------------------------------------------------------
# Bootloader
#-----------------------------------------------------------------------

$(BL_HEX_FILE_PATH): keys \
 nrfsdk/external/micro-ecc/nrf52hf_armgcc/armgcc/micro_ecc_lib_nrf52.a
	@make -s -C pinetime/bootloader

bootloader.hex: $(BL_HEX_FILE_PATH)
	@cp $< $@

.PHONY: clean_bootloader

clean_bootloader:
	@rm -f bootloader.hex
	@make -s -C pinetime/bootloader clean

#-----------------------------------------------------------------------
# Application
#-----------------------------------------------------------------------

pinetime/application/_build/nrf52832_xxaa.hex:
	@make -s -C pinetime/application

application.hex: pinetime/application/_build/nrf52832_xxaa.hex
	@cp $< $@

.PHONY: clean_application

clean_application:
	@rm -f application.hex
	@make -s -C pinetime/application clean

#-----------------------------------------------------------------------
# Flashing
#-----------------------------------------------------------------------

flash_erase_all:
	@echo "*******************************************************"
	@echo "*  YOU ARE ABOUT TO ERASE THE SOFTDEVICE, BOOTLOADER  *"
	@echo "*  AND APPLICATION FROM THE PINETIME.                 *"
	@echo "*******************************************************"
	@read -p "PRESS CTRL-C TO ABORT OR ENTER TO ERASE ALL: " DUMMY
	@pyocd erase -t nrf52 --mass-erase

flash_bootloader: bootloader.hex
	@echo "*********************************************************"
	@echo "*  YOU ARE ABOUT TO FLASH A BOOTLADER ON THE PINETIME.  *"
	@echo "*********************************************************"
	@read -p "PRESS CTRL-C TO ABORT OR ENTER TO FLASH THE BOOTLOADER: " DUMMY
	@pyocd flash -t nrf52 bootloader.hex

flash_softdevice: $(SD_HEX_FILE_PATH)
	@echo "********************************************************"
	@echo "*  YOU ARE ABOUT TO FLASH A SOFTDEVICE ON THE PINETIME *"
	@echo "********************************************************"
	@read -p "PRESS CTRL-C TO ABORT OR ENTER TO FLASH THE SOFTDEVICE: " DUMMY
	@pyocd flash -t nrf52 $(SD_HEX_FILE_PATH)

flash_erase_and_flash_all: bootloader.hex $(SD_HEX_FILE_PATH)
	@echo "***************************************************"
	@echo "*  YOU ARE ABOUT TO ERASE THE PINETIME AND FLASH  *"
	@echo "*  A BOOTLOADER AND SOFTDEVICE ON IT.             *"
	@echo "***************************************************"
	@read -p "PRESS CTRL-C TO ABORT OR ENTER TO ERASE AND FLASH ALL: " DUMMY
	@pyocd erase -t nrf52 --mass-erase
	@pyocd flash -t nrf52 bootloader.hex
	@pyocd flash -t nrf52 $(SD_HEX_FILE_PATH)

#-----------------------------------------------------------------------
# Packaging
#-----------------------------------------------------------------------

bootloader.zip: bootloader.hex
	@nrfutil pkg generate --hw-version 52 --sd-req 0xCB --bootloader-version $(BL_VER) --bootloader bootloader.hex --key-file secret_key.pem bootloader.zip

application.zip: application.hex keys
	@nrfutil pkg generate --hw-version 52 --sd-req 0xCB --application-version $(BL_VER) --application application.hex --key-file secret_key.pem application.zip

#-----------------------------------------------------------------------
# General cleanup
#-----------------------------------------------------------------------

.PHONY: clean

clean: clean_application clean_bootloader clean_micro_ecc
	@rm -f bootloader.zip
	@rm -f application.zip
	@rm -f softdevice.zip
