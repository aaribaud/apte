# A PineTime Environment

## Introduction

### What is APTE?

APTE is a software build environment on the Pine Time dev kit,
heavily based on the nRF SDK, and aimed at simplifying development and
deployment of PineTime bootloaders and applications.

### What can I do with APTE?
 
  * Build a bootloader
  * Build an application
  * Flash a SoftDevice and bootloader through the dev kit SWD
    interface
  * Update a bootloader or application through BLE DFU
  * Create your own bootloader or application based on the ones provided

### How do I use APTE to flash my new dev kit?

Assuming you have a brand new PineTime dev kit, it probably came with
factory software on it (it shows a watchface with hands when you press the
button). In order to replace it with APTE-built firmware, you must:

  * clone this repository
  * install the prerequisites
  * connect your PC to the kit via a pyOCD-supported probe
  * run the folliwong command:

<code bash>$ make erase_and_flash_all
</code>

This will:

  * Select a default SoftDevice
  * Build the bootloader for this SoftDevice
  * Erase the dev kit's flash memory
  * Flash the SD and bootloader
  * Build the application for this SoftDevice
  * Package the application for flashing through the BLE DFU

### What else can APTE do?

See [detailed description](#detailed_description).

## <a id="requirements">Requirements</a>

APTE needs:

  * A development host with a reasonably recent Linux distro
  * A GCC toolchain to build ARM Cortex M code
  * *GNU make*
  * *nrfutil* for managing update packages and signing keys 
  * *pyOCD* for SWD flashing

## Setup example

The following applies to setting up a Xubuntu 18.04 system for use with APTE,
but can probably serve as a guideline for other systems.

## Setting up the GCC toolchain

Install the `gcc-arm-none-eabi` toolchain from the Uubuntu repositories:

<code bash>$ sudo apt install gcc-arm-none-eabi
</code>

## Setting up the GCC toolchain

Install *GNU Make* from the Uubuntu repositories:

<code bash>$ sudo apt install make
</code>

## Setting up nrfutil

**IMPORTANT - At the time of this writing, nrfutil is available for Python 2
and 3, but the version installed for Python 3 fails to execute properly. For
now we have to use the Python 2 package.**

First, if `pip` is not already on your system, install it from the Ubuntu
repositories:

<code bash>$ sudo apt install python-pip
</code>

Second, use `pip` to install `nrfutil`:
repositories:

<code bash>$ pip install nrfutil
</code>

## Setting up pyOCD

First, if `pip3` is not already on your system, install it from the Ubuntu
repositories:

<code bash>$ sudo apt install python3-pip
</code>

Second, use `pip3` to install `nrfutil`:
repositories:

<code bash>$ pip3 install pyocd
</code>

# Detailed description

## Help

Every use of APTE corresponds to a target in its Makefile.

You can get a list of all available targets alng with their short
description by running:
 
<code bash>$ make help
</code>

## The bootloader

The bootloader (along with the SoftDevice) is a mandatory part of the
PineTime SW which executes at power-on and on every reset.

Upon execution, the bootloader checks for a valid application and, if there is
none, enters DFU mode to allow flashing a new one.

If there is a valid application, the bootloader then protects its own flash
sectors from erasure, as well as the UICR region, then passes control to the
application.

The bootloader will also enter DFU if the PineTime button is depressed at boot.
This allows recovering control from a misbehaving application by forcing it to
reset while pressing the button. If the application can not reset at will,
[see the FAQ](#faq-recovery).

When DFU is entered manually, the bootloader remain in DFU mode for 2 minutes
then passes control to the application if there is a valid one.

### Building the bootloader

<code bash>$ make bootloader.hex
</code>

builds the open bootloader and puts it in `bootloader.hex`.

### Flashing the bootloader

<code bash>$ make flash_bootloader
</code>

builds the bootloader if needed, then flashes `bootloader.hex` via pyOCD.

## The application

The application is a simple demo. Right now, it just pulses the backlight
every second, and pressing the button toggles the backlight on and off.

Also, if the button is pressed more than 10 seconds, the application resets
into the bootloader, which will enter DFU.

Finally, the application also implements buttonless DFU: you can connect to
it with e.g. the nRF Toolbox and initiate a SW update.

### Building the application

This application is built by running:

<code bash>$ make application.hex
</code>

### Flashing the application

The application is flashed through BLE DFU. This requires `application.hex`
to be packaged into a zip file, namely `application.zip`. Just type

<code bash>$ make application.zip
</code>
 
Then uploading it to the PineTime via the nRF tools.

You can run the nRF tools on your PC, but it requires a development kit from
Nordic to serve as the BLE host for connection to the PineTime -- the nRF tools
do not make use of the PC's BLE interface.

If, like me, you don't have a Nordic nRF dev kit, then you can simply send
the `application.zip` to a mobile phone on which you have installed the nRF
Toolbox. Then, with the Toolbox, you can send the file to the PineTime via
BLE DFU.

Note: when selecting the BLE device for DFU, look for "PineTime" if the
device already has an application, or "DfuPintTime" if it does not.


## FAQ

**Q**: Why use DFU for the application? not simply flash the application.hex
file through SWD?

**A**: The bootloader only accepts an application if it can tell it's
for the right SoftDevice, and that information is not in the hex file, only 
the zip file. That being said, support for flashing HEX application may be
added in the future.

**Q**: I cannot find DfuPineTime or PineTime in the device list of the nRF
Toolbox. Why?

**A**: Did you give the nRF Toolbox access to the phone's location? Yes, I
know its weird and it should not be, but apparently access to the location is
required by Android in order to see Bluetooth devices.

**Q**: The nRF Toolbox has access to the phone's location, but I **still*
cannot find DfuPineTime or PineTime in the device list of the nRF
Toolbox. Why?

**A**: Try pressing the PineTime button for at least 10 seconds, it *should*
reset and enter DFU mode.

**Q**: I tried the 10 seconds press, still no luck.

**A** There is a last-resort recovery procedure:
  * Let the battery drain until the PineTime is completely off
  * Depress the button and keep it depressed
  * Put the watch on its cradle
  * Wait for a sign of life from the bootloader
  * Release the button
