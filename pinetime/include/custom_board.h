/**
 * Copyright (c) 2014 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef PINETIME_DK_H
#define PINETIME_DK_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"

#define SPIM1_SCK_PIN        2   // SPI-SCK, LCD_SCK
#define SPIM1_MOSI_PIN       3   // SPI-MOSI, LCD_SDI
#define SPIM1_MISO_PIN       4   // SPI-MISO
#define SPIM1_SS_PIN         5   // SPI-CE# (SPI-NOR)

#define TWI_SDA_PIN          6   // BMA421-SDA, HRS3300-SDA, TP-SDA
#define TWI_SCL_PIN          7   // BMA421-SCL, HRS3300-SCL, TP-SCL
#define BMA421_INT_PIN       8   // BMA421-INT

#define LCD_DET_PIN          9   // LCD_DET
#define TP_RESET_PIN        10   // TP_RESET

#define CHRG_IND_PIN        12   // CHARGE INDICATION
#define PUSH_BTN_IN_PIN     13   // PUSH BUTTON_IN
#define LCD_BL_LOW_PIN      14   // LCD_BACKLIGHT_LOW
#define PUSH_BTN_OUT_PIN    15   // PUSH BUTTON_OUT
#define VIBRATOR_OUT_PIN    16   // VIBRATOR OUT

#define LCD_RS_PIN          18   // LCD_RS
#define PWR_PRES_IND_IN_PIN 19   // POWER PRESENCE INDICATION

#define LCD_BL_MID_PIN      22   // LCD_BACKLIGHT_MID
#define LCD_BL_HIGH_PIN     23   // LCD_BACKLIGHT_HIGH
#define _3V3_PWR_CTL_PIN    24   // 3V3 POWER CONTROL
#define LCD_CS_PIN          25   // LCD_CS
#define LCD_RESET_PIN       26   // LCD_RESET

#define TP_INT_PIN          28   // TP_INT

// LEDs definitions for PineTime Dev Kit
#define LEDS_NUMBER    3

#define LED_START      14
#define LED_1          14
#define LED_2          22
#define LED_3          23
#define LED_STOP       23

#define LEDS_ACTIVE_STATE 0

#define LEDS_INV_MASK  LEDS_MASK

#define LEDS_LIST { LED_1, LED_2, LED_3 }

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2
#define BSP_LED_2      LED_3

#define BUTTONS_NUMBER 1

#define BUTTON_START   13
#define BUTTON_1       13
#define BUTTON_STOP    13
#define BUTTON_PULL    NRF_GPIO_PIN_NOPULL

#define BUTTONS_ACTIVE_STATE 1

#define BUTTONS_LIST { BUTTON_1 }

#define BSP_BUTTON_0   BUTTON_1

#ifdef __cplusplus
}
#endif

#endif // PINETIME_DK_H
